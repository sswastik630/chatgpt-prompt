const express = require("express");
const GetResponse = require("./GetResponse");

const router = express.Router();

router.post('/', async (request, response, next) => {
    const useCase = GetResponse.create(request, response);
    useCase.execute();
})

// router.post('/', (req, res)=>{
//     res.send("Wrong InputsS");
// })

module.exports = router;