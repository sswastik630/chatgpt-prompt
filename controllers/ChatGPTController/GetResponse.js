require('dotenv').config();
const chatgptutils = require('../../utils/ChatGPT/chatgptutils')

module.exports = class GetResponse {
  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

  validate() {

  }

  async execute() {
    const { question } = this.request.body;

    const chatgptresponse = await chatgptutils.chatgptquery(question);
    if (chatgptresponse.status == 200){
        return this.response.send({data: chatgptresponse.data.choices})
    }
    else{
        return this.response.send({data: null, status: 404})
    }
  }

  static create(request, response) {
    const useCase = new GetResponse(request, response);
    return useCase;
  }
};
