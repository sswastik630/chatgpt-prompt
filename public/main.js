const submitBtn = document.getElementById("submit-btn");
const inputBox = document.getElementById("input");
const outputContainer = document.getElementById("output-container");
const errorContainer = document.getElementById("error-container");
const spinner = document.querySelector('.spinner');

submitBtn.addEventListener("click", async () => {
  const inputText = inputBox.value.trim();
  spinner.style.display = 'block';
  if (inputText === "") {
    errorContainer.textContent = "Please enter a valid question.";
    outputContainer.textContent = "";
    return;
  }
  const response = await fetch(`/api/chat`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ question: inputText })
  })
  spinner.style.display = 'none';
  console.log(inputText);
  console.log(response);
  if (response.ok) {
    const responseData = await response.json()
    const apiResponse = responseData.data[0]
    const answer = apiResponse.text.trim()
    console.log(answer);
    outputContainer.textContent = answer;
  } else {
    outputContainer.textContent = "";
    errorContainer.textContent = "Error: " + response.statusText;
  }
});
