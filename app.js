const express = require("express");
const RoutesHandler = require('./routes/RoutesHandler')
const bodyParser = require('body-parser');
const cors = require('cors')
require('dotenv').config();

class Application {

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.#middlewares(this.app);
    this.#server(this.app);
    this.#routes(this.app);
  }

  #middlewares(app) {
    app.use(cors({maxAge: 7200}));
    app.options('*', cors({maxAge: 7200}));
    app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));
    app.use(bodyParser.json({ limit: '20mb', verify: (req, res, buf) => { req.rawBody = buf; }}));
    app.use(express.static('public'));
  }

  #server(app) {
    app.listen(this.port, () => {
      console.log(`Server started on port ${this.port}\n Visit link: https:\\localhost:${this.port}\\`);
    });
  }

  #routes(app) {
    RoutesHandler.routes(app);
  }
}

const application = new Application();
