const { Configuration, OpenAIApi } = require("openai");

const chatgptquery = async (question) => {
  const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
  });
  const openai = new OpenAIApi(configuration);
  const chatgptresponse = await openai.createCompletion({
    model: "text-davinci-003",
    prompt: question,
    temperature: 0,
    max_tokens: 1000,
  });
  return chatgptresponse;
};

module.exports = {
    chatgptquery
}
