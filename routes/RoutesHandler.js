const ChatGPTController = require("../controllers/ChatGPTController/routes");

class RoutesHandler {
  routes(app) {
    app.use("/api/chat", ChatGPTController);
    app.get("/", (req, res) => {
      // Send the index.html file
      res.sendFile(__dirname + "/public/index.html");
    });
  }
}

const routesHandler = new RoutesHandler();

module.exports = routesHandler;
